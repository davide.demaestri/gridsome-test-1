// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`


const axios = require('axios')

module.exports = function (api) {
  api.loadSource(async actions => {
    const collection = actions.addCollection('CeparPost')

    const { data } = await axios.get('https://api.hubapi.com/content/api/v2/blog-posts?hapikey=6adf8ca9-bc4b-4a1d-804d-d603e15cbaf6')

    for (const item of data.objects) {
      collection.addNode({
        id: item.id,
        slug: item.slug,
        date: item.publish_date,
        title: item.page_title,
        excerpt: item.post_summary,
        thumbnail: item.post_featured_image_if_enabled,
        content: item.post_body
      })
    }
  })
}